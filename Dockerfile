FROM python:3.6-alpine3.7 AS base
COPY requirements.txt /
WORKDIR /
RUN apk update \
    && apk add --no-cache g++ tzdata \
	&& apk add python py-pip curl unzip libexif udev chromium chromium-chromedriver xvfb \
    && cp /usr/share/zoneinfo/Asia/Jakarta /etc/localtime \
    && echo "Asia/Jakarta" > /etc/timezone \
    && pip install --upgrade pip \
    && pip install --no-cache-dir -r requirements.txt \
    && apk del g++ tzdata


FROM base
COPY ./ /app
WORKDIR /app
ENTRYPOINT ["python", "api.py"]
