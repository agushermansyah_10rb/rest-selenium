from typing import List, Union
from library.db import Db


class Model:
    table = None
    pk = 'id'

    def __init__(self, conn, cursor):
        self.db = Db(conn=conn, cursor=cursor)

    def __get(self, data: dict = None, field='*'):
        self.db.select(self.table, field=field)
        self.where(data)
        return self.db.execute(commit=True)

    def where(self, data: Union[dict, str], operator='AND'):
        if data:
            if isinstance(data, dict):
                for key, value in data.items():
                    self.db.exact_where(key, value, operator)
            elif isinstance(data, str):
                self.db.where(data, operator)
        return self

    def order(self, by, reverse=False):
        self.db.order(by, reverse)
        return self

    def random(self):
        self.db.order(random=True)
        return self

    def limit(self, limit, offset=None):
        self.db.limit(offset, limit)
        return self

    def join(self, model, foreign_key=None, owner_key=None, join_type='left'):
        if not foreign_key:
            foreign_key = f'{model.table}_{model.pk}'
        if not owner_key:
            owner_key = model.pk
        on = f'{self.table}.{foreign_key}={model.table}.{owner_key}'
        self.db.join(model.table, model.table, on=on, join_type=join_type)
        return self

    def find(self, pk, field='*') -> dict:
        return self.__get({f'{self.table}.{self.pk}': pk}, field).fetchone

    def find_or_fail(self, pk, field='*') -> dict:
        found = self.find(pk, field)
        if not found:
            raise NotFoundException()
        return found

    def get(self, data: dict = None, field='*') -> List[dict]:
        return self.__get(data, field).fetchall

    def first(self, data: dict = None, field='*') -> dict:
        self.db.limit(limit=1)
        return self.__get(data, field).fetchone

    def first_or_fail(self, data: dict = None) -> dict:
        first = self.first(data)
        if not first:
            raise NotFoundException()
        return first

    def paginate(self, limit, offset=None, field='*') -> dict:
        self.limit(limit, offset)
        result = self.__get(field=field)
        return dict(rows=result.fetchall, total=result.rowscount, offset=offset, limit=limit)

    def insert(self, data, is_ignore=None, is_update_field_id=None):
        self.db.insert(self.table, data, is_ignore, is_update_field_id)
        return self.db.execute()

    def insert_ignore(self, data):
        return self.insert(data, is_ignore=True)

    def upsert(self, data):
        return self.insert(data, is_update_field_id=['id'])

    def update(self, pk, data):
        self.where({self.pk: pk})
        self.db.update(self.table, data)
        return self.db.execute()

    def delete(self, pk=None, **kwargs):
        if pk:
            self.where({self.pk: pk})
        if kwargs:
            self.where(kwargs)
        self.db.delete(self.table)
        return self.db.execute()
