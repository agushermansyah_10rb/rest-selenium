import argparse
import logging
from configparser import ConfigParser

from flask import Flask, request
from gevent.pywsgi import WSGIServer

from library.logger import setup_logging
from helper import success_response, error_response

from controller.browser import BrowserController


class App(Flask):

    def __init__(self, import_name, **kwargs):
        super().__init__(import_name)
        config = ConfigParser()
        config.read(kwargs.get('config'))
        self.bw_controller = BrowserController(config=config)


app = App(__name__, config='config.ini')
setup_logging()
logger = logging.getLogger(__name__)


@app.route('/get_session', methods=['GET'])
def get_session():
    try:
        result = app.bw_controller.get_session()
        return success_response(result, 'success')
    except Exception as e:
        return error_response(str(e))


@app.route('/login', methods=['POST'])
def login():
    try:
        url = request.form.get('url')
        session_id = request.form.get('session_id')
        user_selector = request.form.get('user_selector')
        user_value = request.form.get('user_value')
        password_selector = request.form.get('password_selector')
        password_value = request.form.get('password_value')
        submit_selector = request.form.get('submit_selector')
        data = dict(
            url=url, session_id=session_id,
            user_selector=user_selector, user_value=user_value,
            password_selector=password_selector, password_value=password_value,
            submit_selector=submit_selector
        )
        result = app.bw_controller.login(
            **{k: v for k, v in data.items() if v is not None}
        )
        return success_response(result, 'success')
    except Exception as e:
        return error_response(str(e))


@app.route('/get_cookies', methods=['GET'])
def get_cookies():
    try:
        url = request.values.get('url')
        session_id = request.values.get('session_id')
        data = dict(url=url, session_id=session_id)
        result = app.bw_controller.get_cookies(
            **{k: v for k, v in data.items() if v is not None}
        )
        return success_response(result, 'success')
    except Exception as e:
        return error_response(str(e))


@app.route('/get_html', methods=['POST'])
def get_html():
    try:
        url = request.form.get('url')
        session_id = request.form.get('session_id')
        cookies = request.form.get('cookies')
        scroll = request.form.get('scroll')
        data = dict(url=url, session_id=session_id, scroll=scroll, cookies=cookies)
        result = app.bw_controller.get_html(
            **{k: v for k, v in data.items() if v is not None}
        )
        return success_response(result, 'success')
    except Exception as e:
        return error_response(str(e))


if __name__ == '__main__':
    argp = argparse.ArgumentParser(description='Web Automation Service')
    argp.add_argument('-p', '--port', dest='port', help='engine port listened', type=int, default=8901)

    args = argp.parse_args()

    logger.info(f'listening to http://0.0.0.0:{args.port}')
    http_server = WSGIServer(('0.0.0.0', args.port), app, log=logger)
    http_server.serve_forever()
