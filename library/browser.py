import os
import random
from datetime import datetime
from selenium import webdriver
from splinter import Browser as WebBrowser


class Browser:

    browser = None
    driver = None
    session_id = None

    def __init__(self, executable_path=None, port=None):
        self.port = port
        self.executable_path = executable_path
        self.user_agent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36'

    def start(self, path_session, session_id=None):
        kwargs = dict()
        if self.port:
            kwargs['port'] = self.port
        if self.executable_path:
            kwargs['executable_path'] = self.executable_path

        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        options.add_argument('disable-gpu')
        options.add_argument('no-sandbox')
        options.add_argument('disable-notifications')
        options.add_argument(f'user-agent={self.user_agent}')
        if session_id:
            self.session_id = session_id
            path = f'{path_session}/{self.session_id}'
            if not os.path.isdir(path):
                raise Exception(f'session id {session_id} invalid')
        else:
            self.session_id = self.session()
        options.add_argument(f'user-data-dir={path_session}/{self.session_id}')
        kwargs['options'] = options
        self.browser = WebBrowser(driver_name='chrome', **kwargs)
        self.browser.driver.set_window_size(1280, 720)
        self.driver = self.browser.driver

    def stop(self):
        if self.browser:
            self.browser.quit()

    def session(self):
        unique = random.randint(100, 999)
        _time = datetime.now().strftime('%Y%m%d-%H%M%S')
        session_id = f'{_time}-{unique}'
        if not os.path.isdir('session'):
            os.mkdir('session')
        return session_id
