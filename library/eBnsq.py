import logging
from urllib.parse import urljoin

import requests

from library.logger import setup_logging

setup_logging()
logger = logging.getLogger(__name__)


class Producer:

    def __init__(self, nsqd_http_address):
        self.nsqd_http_address = nsqd_http_address
        self.session = requests.session()

    def publish(self, message, topic, defer=10000):
        request = requests.PreparedRequest()
        request.prepare_url(urljoin(self.nsqd_http_address, 'pub'), params=dict(topic=topic, defer=defer))
        self.session.post(request.url, message)
