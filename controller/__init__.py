from configparser import ConfigParser
from typing import Union
from library.browser import Browser


class Controller(Browser):

    def __init__(self, config: Union[str, ConfigParser] = 'config.ini', **kwargs):
        super(Controller, self).__init__(**kwargs)
        if isinstance(config, str):
            self.config = ConfigParser()
            self.config.read(config)
        else:
            self.config = config
