import logging
import time
import json
import os
import shutil
import pickle

from controller import Controller
from library.logger import setup_logging

setup_logging()
logger = logging.getLogger(__name__)


class BrowserController(Controller):
    default_wait = 3

    def __init__(self, **kwargs):
        super(BrowserController, self).__init__(**kwargs)
        self.path_session = self.config.get('session', 'path')

    def get_session(self):
        self.start(self.path_session)
        return self.session_id

    def login(self, **kwargs):
        url = kwargs.get('url')
        session_id = kwargs.get('session_id')
        user_selector = kwargs.get('user_selector')
        user_value = kwargs.get('user_value')
        password_selector = kwargs.get('password_selector')
        password_value = kwargs.get('password_value')
        submit_selector = kwargs.get('submit_selector')
        assert url, 'url is required'
        assert user_selector, 'user selector is required'
        assert user_value, 'user value is required'
        assert password_selector, 'password selector is required'
        assert password_value, 'password value is required'
        assert submit_selector, 'submit selector is required'
        try:
            self.start(self.path_session, session_id)
            self.visit_url(url)
            user = self.driver.find_element_by_css_selector(user_selector)
            user.clear()
            user.send_keys(user_value)
            pswd = self.driver.find_element_by_css_selector(password_selector)
            pswd.clear()
            pswd.send_keys(password_value)
            submit = self.driver.find_element_by_css_selector(submit_selector)
            submit.click()
            result = dict(login=True, session_id=self.session_id)
            return result
        except Exception as e:
            raise e
        finally:
            self.stop()

    def get_cookies(self, **data):
        try:
            url = data.get('url')
            session_id = data.get('session_id')
            assert url, 'url is required'
            assert session_id, 'session_id is required'
            self.start(self.path_session, session_id)
            self.visit_url(url)
            result = self.driver.get_cookies()
            return result
        except Exception as e:
            raise e
        finally:
            self.stop()

    def get_html(self, **data):
        try:
            url = data.get('url')
            session_id = data.get('session_id')
            cookies = data.get('cookies')
            scroll = data.get('scroll')
            assert url, 'url is required'
            self.start(self.path_session, session_id)
            self.visit_url(url)
            if cookies:
                self.set_cookies(cookies)
                self.visit_url(url)
            if scroll:
                self.scroll_to_bottom()
            result = dict(html=self.page_source(), session_id=self.session_id)
            return result
        except Exception as e:
            raise e
        finally:
            self.stop()

    def delete_session(self, session_id):
        path = f'{self.path_session}/{session_id}'
        if os.path.isdir(path):
            shutil.rmtree(path)
        return True

    def visit_url(self, url):
        self.driver.get(url)
        time.sleep(self.default_wait)

    def scroll_to_bottom(self):
        self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(self.default_wait)

    def page_source(self):
        return self.driver.page_source

    def set_cookies(self, cookies):
        try:
            cookies = json.loads(cookies)
        except:
            try:
                cookies = pickle.loads(cookies)
            except:
                raise Exception('Failed to load cookies')
        try:
            for cookie in cookies:
                cookie = {
                    "name": cookie.get('name'),
                    "value": cookie.get('value'),
                    "path": cookie.get('path', '/'),
                    "secure": cookie.get('secure', 'false')
                }
                self.browser.driver.add_cookie(cookie)
        except:
            raise Exception('Invalid cookies')
