import logging
from datetime import datetime, date
from decimal import Decimal

from library.logger import setup_logging
from urllib.parse import urlparse, urlencode

setup_logging()
logger = logging.getLogger(__name__)


def json_build(data):
    if isinstance(data, dict):
        for k, v in data.items():
            data[k] = json_build(v)
    if isinstance(data, list):
        for i, v in enumerate(data):
            data[i] = json_build(v)
    if isinstance(data, tuple):
        data = json_build(list(data))
    if isinstance(data, datetime):
        data = str(data)
    if isinstance(data, date):
        data = str(data)
    if isinstance(data, Decimal):
        data = str(data)
    return data


def response(message=None, status=None, data=None):
    return json_build(dict(message=str(message) if message else message, status=status, data=data))


def success_response(data=None, message=None):
    return response(message, True, data)


def error_response(message):
    logger.error(message, exc_info=True)
    return response(message, False)


def url_build(url, params: dict):
    return f"{url}{('&' if urlparse(url).query else '?')}{urlencode(params)}"
