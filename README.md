[![Python 3.6](https://img.shields.io/badge/python-3.6-blue.svg)](https://www.python.org/downloads/release/python-360/)

# Introducing
Tiktok Service API merupakan service berupa `API` yang digunakan sebagai antar muka untuk sistem smart-rawler.

## How to install
 ```
pip install -r requirements.txt
```

## How to run
```
python api.py
```
Jalankan menggunakan gunicorn
```
gunicorn -b 0.0.0.0:8800 -w 6 api:app
``` 

## Need help?
```
python main.py --help
```

## Build docker image
```
docker build -t 192.168.20.26:5000/crawler/smart-crawler/tiktok-api ./
```

### Running using docker
```
docker run -v /path/to/config.ini:/app/config.ini -p 8000:8800 --entrypoint python 192.168.20.26:5000/crawler/smart-crawler/tiktok-api api.py
```
Jalankan menggunkan gunicorn
```
docker run -v /path/to/config.ini:/app/config.ini -p 8000:8800 --entrypoint gunicorn 192.168.20.26:5000/crawler/smart-crawler/tiktok-api -b 0.0.0.0:8800 -w 6 api:app
```

## Deploy to Kubernetes
```
kubectl apply -f deployment/
```

### Deploy to Kubernetes using [skaffold](https://skaffold.dev/)
```
skaffold run
```

# Config Reference
Lihat [config.ini.example](config.ini.example)
