import json

from flask import Flask, request
from flask_socketio import SocketIO, emit

from controller.browser import BrowserController

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)


@app.route('/', methods=["GET"])
def index():
    return json.dumps({'status': 'OK'})


@socketio.on('connect')
def connect():
    print(f'Client connecting', request.remote_addr)


@socketio.on('disconnect')
def disconnect():
    print(f'Client {request.remote_addr} disconnected')


@socketio.on('set_browser')
def set_browser(data):
    result = browser.do_activity(**data)
    emit('result', result)


if __name__ == '__main__':
    browser = BrowserController()
    socketio.run(app, host="0.0.0.0", port=1810)
