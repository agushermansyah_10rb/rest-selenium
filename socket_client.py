import socketio
import asyncio


class Main:

    data = None

    def __init__(self, base_url):
        self.base_url = base_url
        self.client = socketio.Client()

    def start(self, params):
        self.client.connect(self.base_url)
        self.client.emit('set_browser', params)
        self.client.on('result', self.handler)

    def handler(self, message):
        self.data = message
        if self.client:
            self.client.disconnect()

    def get_data(self):
        return self.data


if __name__ == '__main__':
    app = Main(base_url='http://172.20.10.2:1810/')
    app.start({
        'url': 'https://www.instagram.com/ahermansyah_/'
    })
    print(app.get_data())

